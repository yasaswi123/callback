const fs = require('node:fs')
const path = require('node:path')


function problem2(){
    try{
        fs.readFile('../lipsum.txt','utf-8',(err,data)=>{
            if(err){
                console.error(err)
            }
            else{
                console.log("reading file successfully")
                const uppercaseText = data.toUpperCase()
                fs.writeFile('./file1.txt',`${uppercaseText}`,(err)=>{
                    if(err){
                        console.error(err)
                    }
                    else{
                        console.log("uppercase file created successfully")
                        fs.appendFile('./filenames.txt','file1.txt\n',(err)=>{
                            if(err){
                                console.error(err)
                            }
                            else{
                                console.log("added data to filenames.txt")
                                fs.readFile('./file1.txt','utf-8',(err,data1)=>{
                                    if(err){
                                        console.error(err)
                                    }
                                    else{
                                        console.log("reading of uppercase file successful")
                                        const lowercaseText = data1.toLowerCase()
                                        const splitting = lowercaseText.split('.')
                                        fs.writeFile('./file2.txt',splitting.toString(),(err)=>{
                                            if(err){
                                                console.error(err)
                                            }
                                            else{
                                                console.log("lowercase file created successfully with splitting")
                                                fs.appendFile('./filenames.txt','file2.txt\n',(err)=>{
                                                    if(err){
                                                        console.error(err)
                                                    }
                                                    else{
                                                        console.log("added next file to filenames.txt")
                                                        fs.readFile('./file2.txt','utf-8',(err,data2)=>{
                                                            if(err){
                                                                console.error(err)
                                                            }
                                                            else{
                                                                console.log("reading of lowercase file was successful")
                                                                const sorting = data2.split(',').sort().toString()
                                                                fs.writeFile('file3.txt',sorting,(err)=>{
                                                                    if(err){
                                                                        console.error(err)
                                                                    }
                                                                    else{
                                                                        fs.appendFile('./filenames.txt','file3.txt',(err)=>{
                                                                            if(err){
                                                                                console.error(err)
                                                                            }
                                                                            else{
                                                                                console.log("added sort file to filenames.txt")
                                                                                fs.readFile('./filenames.txt','utf-8',(err,data)=>{
                                                                                    if(err){
                                                                                        console.error(err)
                                                                                    }
                                                                                    else{
                                                                                        data.split('\n').forEach(eachFile=>{
                                                                                            fs.unlink(eachFile,(err)=>{
                                                                                                if(err){
                                                                                                    console.error(err)
                                                                                                }
                                                                                                else{
                                                                                                    console.log("file deleted successfully")
                                                                                                }
                                                                                            })
                                                                                        })
                                                                                    }
                                                                                })
                                                                            }
                                                                        })
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
    catch(error){
        console.error(error)
    }
}

module.exports = problem2