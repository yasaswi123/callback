const fs = require('node:fs')
const path = require('node:path')

function problem1(numberOfFiles){
    try{
        const directoryPath = './sample'
        fs.mkdir(directoryPath,(err)=>{
            if(err){
                console.error(err)
            }
            else{
                console.log("directory created successfully");
                for(let index = 0 ; index < numberOfFiles;index++){
                    let fileName = `randomFile${index+1}.json`
                    let filePath = path.join(directoryPath,fileName)
                    fs.writeFile(filePath,`File${index+1} created successfully`,(err)=>{
                        if(err){
                            console.error(err)
                        }
                        else{
                            console.log("file created successfully")
                            fs.unlink(filePath,(err)=>{
                                if(err){
                                    console.error(err)
                                }
                                else{
                                    console.log("File deleted successfully")
                                }
                            })
                        }
                    })
                }
            }
        })
    }
    catch(error){
        console.error(error)
    }
}

module.exports = problem1

